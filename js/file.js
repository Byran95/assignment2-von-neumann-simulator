/**
 * Created by Bryan on 24-5-2016.
 */
(function () {
    "use strict";

    window.app = {
        runSpeed: 1000,
        memoryRows: 15,
        stepCounter : 0,
        currentInstruction: {opCode: "", operand: ""},
        currentRegister: 0,
        halt: false,
        isRunning: false,
        memoryLine: 0,

        registers : {
            r0: 0,
            r1: 0
        },

        components: [
            {name: "PC",    val: 0,     label: "Program Counter"},
            {name: "INC",   val: null,  label: "Incrementer"},
            {name: "IR",    val: null,  label: "Instruction Register"},
            {name: "CU",    val: null,  label: "Control Unit"},
            {name: "MAR",   val: null,  label: "Memory Address Register"},
            {name: "MBR",   val: null,  label: "Memory Buffer Register"},
            {name: "R",     val: null,  label: "Register"},
            {name: "ALU",   val: null,  label: "ALU"},
            {name: "RAM",   val: null,  label: "Memory (RAM)"},
            {name: "Program", val: null, label: "Program (Assembly)"}
        ],

        instructions: [
            {instr: "load",      bitValue: "00000001"},         // required
            {instr: "store",     bitValue: "00001011"},        // required
            {instr: "copy"},         // optional
            {instr: "move"},         // required
            {instr: "add",       bitValue: "00101001"},          // required
            {instr: "sub"},          // required
            {instr: "and"},          // required
            {instr: "or"},           // required
            {instr: "lshift"},       // optional
            {instr: "rshift"},       // optional
            {instr: "blt"},          // master
            {instr: "ble"},          // master
            {instr: "bgt"},          // master
            {instr: "bge"},          // master
            {instr: "beq"},          // master
            {instr: "bne"},          // master
            {instr: "jump"},         // optional
            {instr: "halt",      bitValue: "10101011"}          // required
        ],

        explanation: [
            {name: "Program Counter",               label: "Stores memory address of next instruction to be executed"},
            {name: "IR: Instruction Register",      label: "Instruction op-code (operation code) and data copied here for execution"},
            {name: "ALU: Arithmetic & Logic Unit",  label: "Can be one of the data processing units"},
            {name: "MAR: Memory Address Register",  label: "Stores copy of memory address to be read"},
            {name: "MBR: Memory Buffer Register",   label: "Stores copy of value to be read from or written to memory"},
            {name: "Data Registers",                label: "Additional registers (e.g. r0r15) may be used to store instructions and/or data"}
        ],

        //Draw functions
        drawTitle: function() {
            var h1Node, divNode;
            divNode = document.createElement('div');
            divNode.setAttribute('class','row');

            h1Node = document.createElement('h1');
            h1Node.innerHTML = 'Von Neumann Simulator';

            divNode.appendChild(h1Node);
            app.container.appendChild(divNode);

        },

        drawExplanation: function() {
            var nameNode, labelNode, divNode, divLabelNode, divNameNode, i;
            divNode = document.createElement('div');
            divNode.setAttribute('class', 'row');

            divNameNode = document.createElement('div');
            divNameNode.setAttribute('id', 'names');

            divLabelNode = document.createElement('div');
            divLabelNode.setAttribute('id', 'labels');

            for(i = 0; i < app.explanation.length; i++) {
                nameNode = document.createElement('p');
                nameNode.innerHTML = app.explanation[i].name;
                divNameNode.appendChild(nameNode);

                labelNode = document.createElement('p');
                labelNode.innerHTML = app.explanation[i].label;
                divLabelNode.appendChild(labelNode);
            }

            divNode.appendChild(divNameNode);
            divNode.appendChild(divLabelNode);

            app.container.appendChild(divNode);

        },

        drawQuickBar: function() {
            var divNode, hrNode, resetButton, nextButton, runButton, speedBar;
            divNode = document.createElement('div');
            divNode.setAttribute('class', 'row');

            hrNode = document.createElement('hr');
            hrNode.setAttribute('class', 'divider');

            resetButton = document.createElement('button');
            resetButton.setAttribute('id', 'resetButton');
            resetButton.setAttribute('type', 'button');
            resetButton.setAttribute('class', 'btn btn-primary');
            resetButton.innerHTML = 'reset';
            resetButton.addEventListener("click", app.resetClickHandler);

            runButton = document.createElement('button');
            runButton.setAttribute('id', 'runButton');
            runButton.setAttribute('type', 'button');
            runButton.setAttribute('class', 'btn btn-primary');
            runButton.innerHTML = 'run';
            runButton.addEventListener("click", app.runClickHandler);

            nextButton = document.createElement('button');
            nextButton.setAttribute('id', 'nextButton');
            nextButton.setAttribute('type', 'button');
            nextButton.setAttribute('class', 'btn btn-primary');
            nextButton.innerHTML = 'next';
            nextButton.addEventListener("click", app.nextClickHandler);

            speedBar = document.createElement('input')
            speedBar.setAttribute('type', 'number');
            speedBar.setAttribute('id', 'speedBar');
            speedBar.setAttribute('value', app.runSpeed.toString());

            divNode.appendChild(runButton);
            divNode.appendChild(speedBar);
            divNode.appendChild(resetButton);
            divNode.appendChild(nextButton);

            app.container.appendChild(divNode);

        },

        drawHr: function() {
            var hrNode;

            hrNode = document.createElement('hr');
            hrNode.setAttribute('class', 'divider');

            app.container.appendChild(hrNode);
        },

        drawRegisters: function() {
            var i, divNode, pNode, container, counter = 0, rowNode, columnOne, columnTwo, columnThree, columnFour, textNode;
            container = document.createElement('div');
            container.setAttribute('class', 'row');

            columnOne = document.createElement('div');
            columnOne.setAttribute('class', 'col-md-3');

            columnTwo = document.createElement('div');
            columnTwo.setAttribute('class', 'col-md-3');

            columnThree = document.createElement('div');
            columnThree.setAttribute('class', 'col-md-4');

            columnFour = document.createElement('div');
            columnFour.setAttribute('class', 'col-md-2');

            for(i = 0; i < app.components.length; i++) {
                rowNode = document.createElement('div');
                rowNode.setAttribute('class', 'row');

                divNode = document.createElement('div');
                divNode.setAttribute('id', app.components[i].name);
                divNode.setAttribute('class', 'register col-md-10');

                pNode = document.createElement('p');
                pNode.setAttribute('id', 'registerName');
                pNode.innerHTML = app.components[i].label;

                textNode = document.createElement('p');
                textNode.setAttribute('class', 'innerText');

                if(app.components[i].name == "PC") {
                    textNode.textContent = app.components[0].val;
                    divNode.className += " highlight";
                }

                divNode.appendChild(pNode);
                divNode.appendChild(textNode);

                rowNode.appendChild(divNode);

                if(counter < 4) {
                    columnOne.appendChild(rowNode);
                } else if(counter < 8) {
                    columnTwo.appendChild(rowNode);
                } else if(counter < 9) {
                    columnThree.appendChild(rowNode);
                } else {
                    columnFour.appendChild(rowNode);
                }
                counter++;
            }

            container.appendChild(columnOne);
            container.appendChild(columnTwo);
            container.appendChild(columnThree);
            container.appendChild(columnFour);
            app.container.appendChild(container);
        },

        drawListInMemory: function() {
            var register, list, listItem;

            register = document.getElementById("RAM");
            list = document.createElement('ol');
            list.setAttribute("id", "orderedMemory");
            list.setAttribute("start", "0");

            for(var i = 0; i < app.memoryRows; i++) {
                listItem = document.createElement('li');
                listItem.setAttribute('id', 'item' + [i]);
                list.appendChild(listItem);
            }

            register.appendChild(list);
        },

        drawListInProgram: function() {
            var register, list, listItem, input;

            register = document.getElementById("Program");
            list = document.createElement('ol');
            list.setAttribute("id", "orderedProgram");
            list.setAttribute("start", "0");
            list.setAttribute("contenteditable", "true");

            for(var i = 0; i < app.memoryRows; i++) {
                listItem = document.createElement('li');
                listItem.addEventListener("keyup", app.readAssembly);

                input = document.createElement('input');
                input.setAttribute("type", "text");
                input.setAttribute('id', 'pItem' + [i]);
                input.setAttribute('class', 'input')

                listItem.appendChild(input);
                list.appendChild(listItem);
            }

            register.appendChild(list);
        },

        highLightRegister: function(registerName) {
            var register, highlightRegister;
            register = document.getElementsByClassName("highlight")[0];

            if(register != [] && register != undefined) {
                register.classList.remove("highlight");
            }

            highlightRegister = document.getElementById(registerName);
            highlightRegister.className += " highlight";
        },

        //Insert data in Program
        insertDataIntoProgram: function() {
            document.getElementById('pItem0').value = 'LOAD R0 12';
            document.getElementById('pItem1').value = 'LOAD R1 13';
            document.getElementById('pItem2').value = 'ADD R0 R1';
            document.getElementById('pItem3').value = 'STORE 14 R0';
            document.getElementById('pItem4').value = 'HALT';
            document.getElementById('pItem12').value = '112';
            document.getElementById('pItem13').value = '113';
        },

        //Insert data in Program
        insertTestDataIntoMemory: function() {
            document.getElementById('item0').innerText = '00000001 00000000 00001100';
            document.getElementById('item1').innerText = '00000001 00000001 00001101';
            document.getElementById('item2').innerText = '00101001 00000000 00000001';
            document.getElementById('item3').innerText = '00001011 00001110 00000000';
            document.getElementById('item4').innerText = '10101011';
            document.getElementById('item12').innerText = '01110000';
            document.getElementById('item13').innerText = '01110001';
        },
        //Clickhandlers
        nextClickHandler: function() {
            if(app.stepCounter == 0) {
                app.step1();
            } else if(app.stepCounter == 1) {
                app.step2();
            } else if(app.stepCounter == 2) {
                app.step3();
            } else if(app.stepCounter == 3) {
                app.step4();
            } else if(app.stepCounter == 4) {
                app.step5();
            } else if(app.stepCounter == 5) {
                app.step6();
            } else if(app.stepCounter == 6) {
                app.step7();
            } else if(app.stepCounter == 7) {
                app.step8();
            } else if(app.stepCounter == 8) {
                app.step9();
            }
        },

        resetClickHandler: function() {
            app.reset();
        },

        runClickHandler: function() {
            var runButton = document.getElementById('runButton');

            if(app.isRunning == false) {
                app.isRunning = true;
                runButton.innerHTML = "pause";
            } else {
                app.isRunning = false;
                runButton.innerHTML = "run";
            }
            app.autoRun();
        },

        //Logic
        getUsedMemoryLine: function() {
            var li = null, ol, position;
            ol = document.getElementById('orderedMemory');

            for (var i = 0; i < ol.getElementsByTagName('li').length; i++) {
                if (ol.getElementsByTagName('li')[i].innerText.trim() != "") {
                    li = ol.getElementsByTagName('li')[i];
                    if(!(li.classList.contains("used"))) {
                        li.className += "used";
                        position = i;
                        break;
                    }
                }
            }

            return position;
        },

        getInstruction: function(value) {
            var bit, instruction;

            bit = value.substring(0, 8);
            for (var i = 0; i < app.instructions.length; i++) {
                if(app.instructions[i].bitValue == bit) {
                   instruction = app.instructions[i].instr;
                }
            }
            return instruction;
        },

        getRegister: function(binary) {
            return parseInt(binary, 2);
        },

        dec2bin: function (dec, exp) {

            var i, iVal, bitStr;
            i = Math.pow(2, exp) - 1,
                bitStr = ""
            ;

            while (i >= 0) {
                iVal = Math.pow(2, i);
                if (dec - iVal >= 0) {
                    bitStr += 1;
                    dec -= iVal;
                } else {
                    bitStr += 0;
                }
                i--;
            }

            return bitStr;
        },

        instr2bin: function (instr) {
            var instrStr = instr.split(" "),
                bitStr;

            for(var i = 0; i < instrStr.length; i++) {
                if (i == 0) {
                    app.instructions.forEach(function(entry) {
                        if(instrStr[i].toLowerCase() == entry.instr) {
                            bitStr = entry.bitValue;
                        }
                    });
                } else {
                    instrStr[i] = instrStr[i].replace(/\D/g, '');
                    bitStr += (" " + app.dec2bin(parseFloat(instrStr[i]), 3));
                }
            }

            return bitStr;
        },

        readAssembly: function(event) {
            var assemblyItem = document.getElementById(event.target.id);
            var id = event.target.id.substr(1).toLowerCase();
            var listItem = document.getElementById(id);
            if (isNaN(assemblyItem.value)) {
                listItem.textContent = app.instr2bin(assemblyItem.value);
            } else if (assemblyItem.value != "") {
                listItem.textContent = app.dec2bin(assemblyItem.value, 3);
            }
            //listItem.innerText = document.getElementById('pItem' + itemNr).innerText;
        },

        editRegisterHtml: function(registerName, innerText) {
            var divNode, pNode = null;
            divNode = document.getElementById(registerName);

            for (var i = 0; i < divNode.childNodes.length; i++) {
                if (divNode.childNodes[i].className == "innerText") {
                    pNode = divNode.childNodes[i];
                    break;
                }
            }

            pNode.innerHTML = innerText;
        },

        //Steps for running program
        //Fetching steps
        step1: function() {
            app.highLightRegister("MAR");
            app.components[4].val =  app.dec2bin(app.getUsedMemoryLine(), 4);
            app.editRegisterHtml('MAR', app.components[4].val);
            app.stepCounter++;
        },

        step2: function() {
            app.highLightRegister("INC");
            app.editRegisterHtml("INC", "+1");
            app.stepCounter++;
        },

        step3: function() {
            app.components[0].val = app.components[0].val + 1;
            app.highLightRegister("PC");
            app.editRegisterHtml("PC", app.components[0].val);
            app.stepCounter++;
        },

        step4: function() {
            var listItem = document.getElementById("item" + (app.components[0].val - 1));
            app.components[5].val = listItem.textContent;

            app.highLightRegister("MBR");
            app.editRegisterHtml("MBR", app.components[5].val);
            app.stepCounter++;
        },

        step5: function() {
            app.currentInstruction.opCode = app.components[5].val.substring(0, 8);
            app.currentInstruction.operand = app.components[5].val.substring(9, 17) + app.components[5].val.substring(18, 26);
            app.components[2].val = app.currentInstruction;
            console.log(JSON.stringify(app.components[2].val));

            app.highLightRegister("IR");
            app.editRegisterHtml("IR", "Op-code: " + app.currentInstruction.opCode + "</br> Operand: " + app.currentInstruction.operand);
            app.stepCounter++;
        },

        //Decode / execute steps
        step6: function() {
            var instruction, binary, temp, register1, register2;
            instruction = app.getInstruction(app.currentInstruction.opCode);

            if(instruction == "load") {
                binary = app.components[2].val.operand.substring(0, 8);
                app.memoryLine = parseInt(app.components[2].val.operand.substring(8, 17), 2);
                app.currentRegister = "r" + app.getRegister(binary);
                temp = instruction + " " + app.currentRegister + " " + app.memoryLine + "</br>";
            } else if (instruction == "add") {
                register1 = app.components[2].val.operand.substring(0, 8);
                register2 = app.components[2].val.operand.substring(8, 17);
                temp = instruction + " r" + app.getRegister(register1) + " r" + app.getRegister(register2) + "</br>";
            } else if (instruction  == "store") {
                app.memoryLine = parseInt(app.components[2].val.operand.substring(0, 8),2);
                app.currentRegister = "r" + app.getRegister(app.components[2].val.operand.substring(8, 17));
                temp = instruction + " " + app.memoryLine + " " + app.currentRegister + "</br>";
            } else if (instruction  == "halt") {
                temp = instruction;
                app.halt = true;
                document.getElementById("nextButton").setAttribute("disabled", "true");
                document.getElementById("nextButton").className += ".disabled";
            }

            if (app.components[3].val == null) {
                app.components[3].val = temp;
            } else {
                app.components[3].val += temp;
            }

            app.highLightRegister("CU");
            app.editRegisterHtml("CU", app.components[3].val);
            app.stepCounter++;
        },

        step7: function() {
            var item = document.getElementById("item" + app.memoryLine),
                value,
                temp = "";

            if(app.getInstruction(app.currentInstruction.opCode) == "load") {

                value = parseInt(item.textContent, 2)

                app.components[6].val = null;

                if(app.currentRegister == "r0") {
                    app.registers.r0 = value;
                } else {
                    app.registers.r1 = value;
                }

                for(var register in app.registers) {
                    console.log(app.registers[register]);
                    temp = register + " = " + app.registers[register] + "</br>";
                    if(app.registers[register] != 0){
                        if(app.components[6].val == null) {
                            app.components[6].val = temp
                        } else {
                            app.components[6].val += temp
                        }

                    }
                }

                app.highLightRegister("R");
                app.editRegisterHtml("R", app.components[6].val);
                app.stepCounter = 0;
            } else {
                app.stepCounter++;
            }
        },

        step8: function() {
            var item = document.getElementById("item" + app.memoryLine);

            if(app.getInstruction(app.currentInstruction.opCode) == "add"){
                app.components[7].val = "r1 + r0 = " + (app.registers.r0 + app.registers.r1);

                app.highLightRegister("ALU");
                app.editRegisterHtml("ALU", app.components[7].val);
                app.stepCounter = 0;
            } else if(app.getInstruction(app.currentInstruction.opCode) == "store") {
                app.highLightRegister("RAM");
                if (app.currentRegister == "r0") {
                    item.innerHTML = app.dec2bin(app.registers.r0, 3);
                } else {
                    item.innerHTML = app.dec2bin(app.registers.r1, 3);
                }
                app.stepCounter = 0;
            }
        },

        //Creating the machine using draw methods
        buildMachine: function() {
            var container;

            container = document.createElement('div');
            container.setAttribute('class', 'container');
            app.container = container;
            document.body.appendChild(app.container);

            app.drawTitle();
            app.drawHr();
            app.drawExplanation();
            app.drawHr();
            app.drawQuickBar();
            app.drawHr();
            app.drawRegisters();
            app.drawHr();
            app.drawListInMemory();
            app.drawListInProgram();

            app.insertTestDataIntoMemory();
            app.insertDataIntoProgram();
        },

        //Reset function
        reset: function() {
            var body = document.getElementsByTagName("body")[0];
            while (body.firstChild) {
                body.removeChild(body.firstChild);
            }

            app.halt = false;
            app.memoryLine = 0;
            app.stepCounter = 0;
            app.currentInstruction = {opCode: "", operand: ""};
            app.currentRegister = 0;
            app.registers = {
                r0: 0,
                r1: 0
            };

            for(var i = 0; i < app.components.length; i++) {
                if(app.components[i].name == "PC") {
                    app.components[i].val = 0;
                } else {
                    app.components[i].val = null;
                }
            }
            app.buildMachine();
        },

        //Run the program until a halt command is found
        autoRun: function() {
            var currentStep;

            app.runSpeed = document.getElementById("speedBar").value;

            if(app.halt == false  && app.isRunning == true) {
                currentStep = "step" + (app.stepCounter + 1);
                app[currentStep](arguments);
                setTimeout(app.autoRun, app.runSpeed);
            } else {
                return;
            }
        },

        //Initialize the application
        init: function() {
            app.buildMachine();
        },

        //Method to run on startup
        run: function() {
            app.init();
        }
    };
}());

window.addEventListener("load",
    app.run
);

